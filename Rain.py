# Rain info
# 
from time import sleep
import json
import socket

from src import rain, mqtt, logger, config, status

CONF = config.loadConfig("/home/pi/weather")
rainInfo = CONF["rain"]
sublocation = CONF["flux"]["sublocation"]

log = logger.beginLogging(rainInfo["log"])
mqtt.startConnection("Rain", log)
HOST = socket.gethostname()


log.info(
	"\n"
	"===========================\n"
	"|| Starting Rain Service ||\n"
	"==========================="
	"\n"
)

while True:
	log.debug("Looking for Rain...")
	totalRain = rain.getTotalRain();

	if(totalRain["buckets"] > 0):
		# Publish to web listeners
		mqtt.publishMessage("weather", json.dumps({ "type":"rain", **totalRain }))
		mqtt.publishMessage("weather/rain", totalRain["inches"])
		
		# Build point for Influx
		try:
			point = "rain,location=system,sublocation={0},host={1} buckets={2},inches={3},mm={4}".format(sublocation, HOST, totalRain["buckets"], totalRain["inches"], totalRain["mm"])
			mqtt.publishMessage("in/rain", point)
			status.sendStatus(mqtt, "rain", True)
		except Exception as e:
			status.sendStatus(mqtt, "rain", false)
			log.error("Unable to calculate buckets")

		rain.reset()

	status.sendStatus(mqtt, "rain", True)
	sleep(rainInfo["sleep"])