import RPi.GPIO as GPIO
import os
import time
from statistics import mean

from . import logger, config

CONF = config.loadConfig("/home/pi/weather")
snowInfo = CONF["snow"]

# Define GPIO to use on Pi
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO_TRIGGER = snowInfo["trigger"]
GPIO_ECHO = snowInfo["echo"]

# TRIGGER_TIME = 0.00001
TRIGGER_TIME = snowInfo["triggerTime"]
MAX_TIME = snowInfo["timeoutTime"]  # max time waiting for response in case something is missed
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)  # Trigger
GPIO.setup(GPIO_ECHO, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # Echo

GPIO.output(GPIO_TRIGGER, False)

# This function measures a distance
def measure(log):
	log.debug("Looking for Object")
	# print("Looking for object")
	# Pulse the trigger/echo line to initiate a measurement
	GPIO.output(GPIO_TRIGGER, True)
	time.sleep(TRIGGER_TIME)
	GPIO.output(GPIO_TRIGGER, False)

	# ensure start time is set in case of very quick return
	start = time.time()
	timeout = start + MAX_TIME

	# set line to input to check for start of echo response
	while GPIO.input(GPIO_ECHO) == 0 and start <= timeout:
		start = time.time()

	if(start > timeout):
		log.debug(GPIO.input(GPIO_ECHO))
		log.debug("Waited too long for response...")
		# print(GPIO.input(GPIO_ECHO))
		# print("Waited too long for response...")
		return -1

	stop = time.time()
	timeout = stop + MAX_TIME
	# Wait for end of echo response
	while GPIO.input(GPIO_ECHO) == 1 and stop <= timeout:
		stop = time.time()

	if(stop <= timeout):
		elapsed = stop - start
		distance = float(elapsed * 34300)/2.0
	else:
		log.debug("Stop time is greater than timeout")
		# print(stop)
		# print("Stop time is greater than timeout")
		return -1

	return distance


def getAvgDepth(log):
	i = 0
	snowDepths = []
	# Take a sample of the snow depths
	while(i < snowInfo["sample"]):
		depth = measure(log)

		# Only add the measurement to the array if:
		# 	measurement came back greater than 0
		# 	measurement came back less than the hard coded base height
		if(depth > -1):
			if(depth < snowInfo["baseHeight"]):
				snowDepths.append(depth)


		# Sleep for some time before running the next set
		# 	This lets us settle the sensor
		# 	This lets us get an average for temp, wind, and sun
		time.sleep(snowInfo["sampleTime"])
		i = i + 1

	log.debug("Total snow depths of: {0}".format(snowDepths))
	if(len(snowDepths) == 0):
		return 0

	avgDepth = 0
	try:
		avgDepth = mean(snowDepths)
	except Exception as e:
		log.error("Unable to find depth")

	return avgDepth


# if __name__ == '__main__':
# 	try:
# 		while True:
# 			distance = measure(1)
# 			if(distance > -1):
# 				print("Measured Distance = %.1f cm" % distance)
# 				print("distance:")
# 				print(distance)
# 			else:
# 				print("#")
# 			time.sleep(0.5)
# 		# Reset by pressing CTRL + C
# 	except KeyboardInterrupt:
# 		print("Measurement stopped by User")
# 		GPIO.cleanup()




# import RPi.GPIO as GPIO                    #Import GPIO library
# import time                                #Import time library
# GPIO.setmode(GPIO.BCM)                     #Set GPIO pin numbering 

# TRIG = 14                                  #Associate pin 15 to TRIG
# ECHO = 15                                  #Associate pin 14 to Echo

# print("Distance measurement in progress")

# GPIO.setup(TRIG, GPIO.OUT)                  #Set pin as GPIO out
# GPIO.setup(ECHO, GPIO.IN)                   #Set pin as GPIO in

# while True:
# 	GPIO.output(TRIG, False)                 #Set TRIG as LOW
# 	print("Waiting For Sensor To Settle")
# 	time.sleep(2)                            #Delay of 2 seconds
# 	print("Sensor settled, Looking for things")

# 	GPIO.output(TRIG, True)                  #Set TRIG as HIGH
# 	time.sleep(0.00001)                      #Delay of 0.00001 seconds
# 	GPIO.output(TRIG, False)                 #Set TRIG as LOW
# 	print("Trigger sent, waiting for reply")

# 	print("GPIO is")
# 	print(GPIO.input(ECHO))
# 	while (GPIO.input(ECHO) == 0):               #Check if Echo is LOW
# 		pulse_start = time.time()              #Time of the last  LOW pulse

# 	while (GPIO.input(ECHO) == 1):               #Check whether Echo is HIGH
# 		pulse_end = time.time()                #Time of the last HIGH pulse 

# 	pulse_duration = pulse_end - pulse_start #pulse duration to a variable

# 	distance = pulse_duration * 17150        #Calculate distance
# 	distance = round(distance, 2)            #Round to two decimal points

# 	if(distance > 20 and distance < 400):     #Is distance within range
# 		print("Distance:", distance - 0.5,"cm")  #Distance with calibration)
# 	else:
# 		print("Out Of Range")                   #display out of range)