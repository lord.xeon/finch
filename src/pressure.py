import board
import adafruit_mprls

i2c = board.I2C()

# Simplest use, connect to default over I2C
mpr = adafruit_mprls.MPRLS(i2c)

def underPressure(mqtt, log):
	log.debug("Checking Pressure")
	try:
		pressure = mpr.pressure
	except RuntimeError:
		log.error("Unable to read MPRLS Sensor")

	log.debug("Air Pressure is: {0}hpa".format(pressure))

	return pressure