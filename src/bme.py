import qwiic_bme280
import sys

bmeSensor = qwiic_bme280.QwiicBme280()

def getDew(humidity):
	return ((100 - humidity) / 5)

def toDegF(tempC):
	return ((tempC * 9) / 5 + 32)


def getWeather(log):
	if (bmeSensor.is_connected() == False) :
		log.error("The BME280 device isn't connected to the system. Please check your connection", \
			file=sys.stderr)
		return {}

	try:
		bmeSensor.begin()
	except OSError as ee:
		log.error("---Unable to connect to BME280!---")
		log.error(ee)
		return {}

	# bmeSensor.set_reference_pressure(PC_PRESSURE)
	tempC = bmeSensor.temperature_celsius
	tempF = bmeSensor.temperature_fahrenheit
	humidity = bmeSensor.humidity
	pressure = bmeSensor.pressure
	altitude = bmeSensor.altitude_feet
	altitudeM = bmeSensor.altitude_meters

	dewC = 0
	dewF = 0
	try:
		dewC = getDew(humidity)
		dewF = toDegF(dewC)
	except Exception as ee:
		log.warn("Unable to convert degrees")
		# log.error(ee)

	return {
		"temperature":{
			"c":format(tempC, ".2f"),
			"f":format(tempF, ".2f")
		},
		"dewPoint":{
			"c":format(dewC, ".2f"),
			"f":format(dewF, ".2f")
		},
		"humidity":format(humidity,".2f"),
		"pressure":format(pressure, ".2f"),
		"altitude":{
			"meters":format(altitudeM, ".2f"),
			"feet":format(altitude, ".2f")
		}
	}
