
import time
import board
import busio
import adafruit_veml6075

i2c = busio.I2C(board.SCL, board.SDA)

veml = adafruit_veml6075.VEML6075(i2c, integration_time=100)


def getUV(log):
	info = {
		"uva":veml.uva,
		"uvb":veml.uvb,
		"index":{
			"uva":0,
			"uvb":0,
			"avg":format(veml.uv_index, ".2f")
		}
	}

	return info
