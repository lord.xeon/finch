from gpiozero import Button
import logging

from . import logger, config

CONF = config.loadConfig("/home/pi/weather")
info = CONF["windSpeed"]

windCount = 0

def resetWind():
	global windCount
	windCount = 0

def getSpin():
	global windCount
	windCount = windCount + 1

def calcSpeed(spins, interval):
	# From Davis Instruments documents
	# V = P*(2.25/T) the speed is in MPh
	# P = no. of pulses per sample period
	# T = sample period in seconds
	windSpeed = spins * (2.25 / interval)

	return {
		"mph":round(windSpeed, 2)
	}

def getWind(interval, log):
	global windCount
	log.debug("Wind Count is: {0} over {1} seconds".format(windCount, interval))
	return calcSpeed(windCount, interval)


windSpeedSensor = Button(info["pin"])
windSpeedSensor.when_pressed = getSpin
