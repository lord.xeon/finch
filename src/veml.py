import time
import veml6075
import smbus

bus = smbus.SMBus(1)

# Example from Pimorni - https://github.com/pimoroni/veml6075-python
# Create VEML6075 instance and set up
uv_sensor = veml6075.VEML6075(i2c_dev=bus)
uv_sensor.set_shutdown(False)
uv_sensor.set_high_dynamic_range(False)
uv_sensor.set_integration_time('100ms')

def getUV(log):
	uva, uvb = uv_sensor.get_measurements()
	uv_comp1, uv_comp2 = uv_sensor.get_comparitor_readings()
	uv_indices = uv_sensor.convert_to_index(uva, uvb, uv_comp1, uv_comp2)

	info = {
		"uva":uva,
		"uvb":uvb,
		"index":{
			"uva":format(uv_indices[0], ".2f"),
			"uvb":format(uv_indices[1], ".2f"),
			"avg":format(uv_indices[2], ".2f")
		}
	}

	return info
