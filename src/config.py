import json


def loadConfig(configLocation):
	with open(configLocation + "/config.json", "r") as f:
			allConfig = json.load(f)

	return allConfig
