import paho.mqtt.client as mqtt
import socket

from . import config

CONF = config.loadConfig("/home/pi/weather")

settings = CONF["mqtt"]
broker = settings["broker"]
clientId = socket.gethostname()

def startConnection(id, logger):
	global clint
	global clientId
	global log
	log = logger
	clientId = id + "@" + clientId
	clint = mqtt.Client(clientId)
	clint.connect(broker)
	# TODO: set up user/pass
	clint.on_connect = beginConnection
	clint.on_disconnect = endConnection

	clint.loop_start()


def beginConnection(client, userData, flags, rc):
	if(rc == 0):
		log.info("Connected to MQTT: {0} from {1}".format(broker, clientId))
	else:
		log.warn("Problem connecting to MQTT: {0} from {1} | reason: {2}".format(broker, clientId, rc))

def endConnection(client, userData, rc):
	log.warn("Lost connection to MQTT: {0} from {1}".format(broker, clientId))
	log.error(rc)


def publishMessage(to, mess):
	log.debug("Publishing to: {0} from {1} -- this: {2}".format(to, clientId, mess))

	result = []
	global clint
	try:
		result = clint.publish(to, mess)
	except Exception as e:
		log.error(e)

	status = result[0]
	if(status == 0):
		log.debug("Successfully sent message to {0} | {1}".format(to, mess))
	else:
		log.warn("Failed to send message to: {0} | reason: {1}".format(to, status))

def publishError(message, critical):
	mess = "error,location=system,sublocation={0},host={1} error={2},critical={3}".format(CONF["flux"]["sublocation"], clientId, message, critical)
	publishMessage("in/error", mess)


def subscribeTo(to, callback):
	global clint
	clint.on_message = callback
	clint.subscribe(to, qos=1)