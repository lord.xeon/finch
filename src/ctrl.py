
import socket
import json
from src import aht, cpu, config, status, adafruit_uv

CONF = config.loadConfig("/home/pi/weather")

sublocation = CONF["flux"]["sublocation"]
HOST = socket.gethostname()


def getWeather(mqtt, log):
	info = aht.getWeather(log)
	log.debug("Got weather - {0}".format(info))
	mqtt.publishMessage("weather", json.dumps({ "type":"temperature", **info }))
	mqtt.publishMessage("weather/temperature", info["temperature"]["c"])
	mqtt.publishMessage("weather/dewpoint", info["dewPoint"]["c"])
	mqtt.publishMessage("weather/humidity", info["humidity"])

	try:
		point = "weather,location=system,sublocation={0},host={1} temperature={2},humidity={3},dewpoint={4}".format(sublocation, HOST, info["temperature"]["c"], info["humidity"], info["dewPoint"]["c"])
		mqtt.publishMessage("in/temperature", point)
		status.sendStatus(mqtt, "weather", True)
	except Exception as e:
		status.sendStatus(mqtt, "weather", False)
		log.error("Unable to send message")

	up = cpu.getUpTime()
	log.debug("Got Uptime: {0}".format(up))
	temp = cpu.getCPU()
	log.debug("Got System Temp: {0}".format(temp))

	tempPoint = "temperature,location=system,sublocation={0},host={1} temperature={2}".format(sublocation, HOST, temp)
	upPoint = "uptime,location=system,sublocation={0},host={1} uptime={2}".format(sublocation, HOST, up)

	mqtt.publishMessage("system/{0}".format(HOST), json.dumps({ "uptime":up, "temperature":temp }))
	mqtt.publishMessage("system/{0}/uptime".format(HOST), up)
	mqtt.publishMessage("system/{0}/systemTemp".format(HOST), temp)
	mqtt.publishMessage("in/system", tempPoint)
	mqtt.publishMessage("in/system", upPoint)


def getUV(mqtt, log):
	info = adafruit_uv.getUV(log)
	log.debug("Got UV - {0}".format(info))
	mqtt.publishMessage("weather", json.dumps({ "type":"UV", **info }))
	mqtt.publishMessage("weather/uva",info["index"]["uva"])
	mqtt.publishMessage("weather/uvb",info["index"]["uvb"])

	try:
		point = "uv,location=system,sublocation={0},host={1} uva={2},uvb={3},indexUvA={4},indexUvB={5},indexAvg={6}".format(sublocation, HOST, info["uva"], info["uvb"], info["index"]["uva"], info["index"]["uvb"], info["index"]["avg"])
		mqtt.publishMessage("in/uv", point)
		status.sendStatus(mqtt, "uv", True)
	except Exception as e:
		status.sendStatus(mqtt, "uv", False)
		log.error("Unable to find UV")
