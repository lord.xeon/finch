
import socket
from datetime import datetime
import json

HOST = socket.gethostname()

def sendStatus(mqtt, service, isUp):
	# now = datetime.now().strftime("%Y-%m-%d::%H-%M-%S")
	# mess = { "service":service, "isAvailable":isUp, "timestamp":now }

	mess = "offline"

	if(isUp):
		mess = "online"

	mqtt.publishMessage("status/{0}/{1}".format(HOST, service), mess)