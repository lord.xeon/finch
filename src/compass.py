import time
import board
from math import atan2, degrees
import adafruit_lis3mdl
#import adafruit_lis2mdl

i2c = board.I2C()
SENSOR = adafruit_lis3mdl.LIS3MDL(i2c)

def vector_2_degrees(x, y):
	angle = degrees(atan2(y, x))
	if(angle < 0):
		angle += 360

	return angle

def getHeading(_sensor):
	magnet_x, magnet_y, _ = _sensor.magnetic
	return vector_2_degrees(magnet_x, magnet_y)

while True:
	print("Heading: {:.2f} degrees".format(getHeading(SENSOR)))

	magX, magY, magZ = SENSOR.magnetic
	print("Magnetometer (gauss): {0:10.3f}, {1:10.3f}, {2:10.3f}".format(magX, magY, magZ))
	print("-")
	time.sleep(2)
