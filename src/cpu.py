##
# System Information
###

from gpiozero import CPUTemperature

import ctypes
import struct

def getUpTime():
	libc = ctypes.CDLL('libc.so.6')
	buf = ctypes.create_string_buffer(4096) # generous buffer to hold struct sysinfo
	if libc.sysinfo(buf) != 0:
		return -1

	uptime = struct.unpack_from('@l', buf.raw)[0]
	return uptime

cpu = CPUTemperature()
def getCPU():
	return cpu.temperature