
import logging

def beginLogging(config):
	logLevel = logging.WARNING
	if(config["level"] == "debug"):
		logLevel = logging.DEBUG
	elif(config["level"] == "info"):
		logLevel = logging.INFO
	elif(config["level"] == "error"):
		logLevel = logging.ERROR
	elif(config["level"] == "crit"):
		logLevel = logging.CRITICAL

	fileHandler = logging.FileHandler(config["file"])
	fileHandler.setFormatter(logging.Formatter(config["format"]))

	logger = logging.getLogger(config["name"])

	logger.setLevel(logLevel)
	logger.addHandler(fileHandler)
	
	return logger
