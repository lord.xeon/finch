
import time
import board
import adafruit_ahtx0

# Create sensor object, communicating over the board's default I2C bus
i2c = board.I2C()  # uses board.SCL and board.SDA
sensor = adafruit_ahtx0.AHTx0(i2c)

sensor.calibrate()

def getDew(humidity):
	return ((100 - humidity) / 5)

def toDegF(tempC):
	return ((tempC * 9) / 5 + 32)

def getWeather(log):
	tempC = 0
	tempF = 0
	dewC = 0
	dewF = 0
	hum = 0

	try:
		tempC = sensor.temperature
		hum = sensor.relative_humidity
	except OSError as ee:
		log.error("---Unable to connect to AHT20!---")
		log.error(ee)
		return {}

	try:
		dewC = getDew(hum)
		dewF = toDegF(dewC)
		tempF = toDegF(tempC)
	except Exception as ee:
		log.warn("Unable to convert degrees")

	return {
		"temperature":{
			"c":format(tempC, ".2f"),
			"f":format(tempF, ".2f")
		},
		"dewPoint":{
			"c":format(dewC, ".2f"),
			"f":format(dewF, ".2f")
		},
		"humidity":format(hum,".2f")
	}
