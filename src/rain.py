import time
import logging
from gpiozero import Button

from . import logger, config

CONF = config.loadConfig("/home/pi/weather")
rainInfo = CONF["rain"]

BUCKET = 0

def tipBucket():
	global BUCKET
	BUCKET = BUCKET + 1
	logging.debug("Total Rain: {0}".format(BUCKET))


def sumRain(buckets):
	inches = buckets * rainInfo["inches"]
	mm = buckets * rainInfo["mm"]

	return {
		"inches":round(inches, 2),
		"mm":round(mm, 1),
		"buckets":buckets
	}

def reset():
	global BUCKET
	BUCKET = 0

def getTotalRain():
	global BUCKET
	return sumRain(BUCKET)

logging.debug("Setting Up Rain sensor on pin: {0}".format(rainInfo["pin"]))
rainSensor = Button(rainInfo["pin"])
rainSensor.when_pressed = tipBucket