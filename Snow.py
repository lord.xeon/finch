# Snow Total Logging
# 
from time import sleep
import json
import socket

from src import snow, mqtt, logger, config, status

CONF = config.loadConfig("/home/pi/weather")
snowInfo = CONF["snow"]
sublocation = CONF["flux"]["sublocation"]

log = logger.beginLogging(snowInfo["log"])
mqtt.startConnection("Snow", log)
HOST = socket.gethostname()


log.info(
	"\n"
	"===========================\n"
	"|| Starting Snow Service ||\n"
	"==========================="
	"\n"
)

def startSnow(client, userData, message):
	print("data:")
	print(userData)
	print("Got message!")
	print(message)

mqtt.subscribeTo("control/snow", startSnow)

sleepTime = snowInfo["sleep"] * 60
# The sensor is placed a certain distance over the ground
baseHeight = snowInfo["baseHeight"]

# However, due to environmental conditions (leaves, grass, existing snow), the distance might be less than the current base
currentBottom = snow.measure(log)
log.info("----Initial Bottom----")
log.info("Current bottom is: {0:.2g}cm".format(currentBottom))

# If the current base is different, we should reset the local variable for the bottom
if(baseHeight != currentBottom):
	log.info("Setting new bottom to: {0:.2g}cm".format(currentBottom))
	baseHeight = currentBottom

# Sleep for 5 seconds before running the calculations
sleep(5)
log.info("Begin Snow watch every {0} seconds".format(sleepTime))
log.info("----Begin----")

status.sendStatus(mqtt, "snow", True)

snowGain = 0

while True:
	distance = snow.getAvgDepth(log)
	# distance = snow.measure(log)

	if(distance > baseHeight):
		log.debug("No Snow Yet, Distance greater: {0:.2g}".format(distance))
		sleep(sleepTime)

	if(distance == baseHeight):
		log.debug("No Snow Yet, Equal Height: {0:.2g}".format(distance))
		sleep(sleepTime)

	# Calculate the current snow base
	totalSnow = baseHeight - distance
	# Calculate the increase in snow since the last measurement
	snowGain = totalSnow - snowGain

	log.debug("Snowfall!: {0:.2g}cm".format(totalSnow))
	log.debug("Snowgain!: {0:.2g}cm".format(snowGain))
	
	if(totalSnow > 0):
		totalMM = format(totalSnow * 10, ".2f")
		totalInches = format(totalSnow * 0.3937, ".2f")
		gainMM = format(snowGain * 10, ".2f")
		gainInches = format(snowGain * 0.3937, ".2f")

		try:
			mqtt.publishMessage("weather", json.dumps({
				"type":"snow",
				"baseHeight": format(baseHeight, ".3f"),
				"distance":format(distance, ".3f"),
				"inches":totalInches,
				"mm":totalMM,
				"gain":{
					"inches":gainInches,
					"mm":gainMM
				}
			}))
			mqtt.publishMessage("weather/snow/baseHeight", format(baseHeight, ".3f"))
			mqtt.publishMessage("weather/snow/totalInches", totalInches)
			mqtt.publishMessage("weather/snow/gainInches", gainInches)
			point = "snow,location=system,sublocation={0},host={1} inches={2},mm={3},gain={4},gainMM={5}".format(sublocation, HOST, totalInches, totalMM, gainInches, gainMM)
			mqtt.publishMessage("in/snow", point)
			status.sendStatus(mqtt, "snow", True)
		except Exception as e:
			status.sendStatus(mqtt, "snow", False)
			log.error("Unable to track snow")

	sleep(sleepTime)
