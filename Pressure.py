# #Air Pressure Sensor
from time import sleep
import json
import socket

from src import config, logger, mqtt, status, pressure

CONF = config.loadConfig("/home/pi/weather")

airInfo = CONF["pressure"]
sublocation = CONF["flux"]["sublocation"]

log = logger.beginLogging(airInfo["log"])
mqtt.startConnection("pressure", log)
HOST = socket.gethostname()


log.info(
	"\n"
	"==================================\n"
	"|| Starting Air Pressure Service ||\n"
	"=================================="
	"\n"
)

while True:
	status.sendStatus(mqtt, "pressure", True)
	hpa = pressure.underPressure(mqtt, log)

	# Save Air Quality Infoto Influx
	point = "pressure,location=system,sublocation={0},host={1} hpa={2}".format(sublocation, HOST, hpa)
	mqtt.publishMessage("in/pressure", point)

	# Publish Pressure to Weather
	mqtt.publishMessage("weather", json.dumps({ "type":"pressure", "hpa":hpa }))
	mqtt.publishMessage("weather/pressure", hpa)

	sleep(airInfo["sleep"])