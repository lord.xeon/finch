# Weather info
#
from time import sleep, time
import statistics
import json
import socket
import logging

from src import mqtt, logger, config, wind, status

CONF = config.loadConfig("/home/pi/weather")
info = CONF["windSpeed"]
sublocation = CONF["flux"]["sublocation"]

log = logger.beginLogging(info["log"])

HOST = socket.gethostname()

mqtt.startConnection("WindSpeed", log)

log.info(
	"\n"
	"================================\n"
	"|| Starting Wind Speed Service ||\n"
	"================================"
)
INTERVAL = info["sleep"]
MAX_GUST = info["gustLength"]

log.info("Monitoring Wind speed in {0} second intervals averaging over {1} seconds".format(INTERVAL, (INTERVAL * MAX_GUST)))
log.info("Connecting to Anemoter")
log.debug("Setting up Wind Speed on pin: {0}".format(info["pin"]))


gustInfo = 0
windSpeeds = []
while True:
	now = time()
	while time() - now < INTERVAL:
		wind.resetWind()
		sleep(INTERVAL)
		speed = wind.getWind(INTERVAL, log)

		gustInfo = gustInfo + 1

		if(speed["mph"] > 0):
			# Store speed for Gust calculation
			windSpeeds.append(speed["mph"])

	if(gustInfo == MAX_GUST):
		# Reset Gust Info
		gustInfo = 0
		log.debug("Wind speeds are: {0}".format(windSpeeds))

		# If there wasn't any wind, don't save anything
		if(len(windSpeeds) == 0):
			continue

		gust = max(windSpeeds)
		avg = round(statistics.mean(windSpeeds), 2)

		if(gust > 0):
			log.debug("Avg Wind speed is {0}mph. Gust is {1}mph".format(avg, gust))
			# Save Wind Speed to Influx
			point = "wind,location=system,sublocation={0},host={1} speed={2},gust={3}".format(sublocation, HOST, avg, gust)
			mqtt.publishMessage("in/wind", point)

			# Publish Wind Speed
			mqtt.publishMessage("weather", json.dumps({ "type":"windSpeed", "avg":avg, "gust":gust, "units":"mph"
				, }))
			mqtt.publishMessage("weather/wind/speed", avg)
			mqtt.publishMessage("weather/wind/gust", gust)
			mqtt.publishMessage("weather/wind/units", "mph")

		# Reset Wind Speed list
		windSpeeds = []
		status.sendStatus(mqtt, "windSpeed", True)
