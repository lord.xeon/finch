from time import sleep
from datetime import datetime
from gpiozero import Button
import json
import socket

import RPi.GPIO as GPIO
from RPi_AS3935 import RPi_AS3935


from src import logger, mqtt, config, status

CONF = config.loadConfig("/home/pi/weather")
config = CONF["lightning"]

log = logger.beginLogging(config["log"])
mqtt.startConnection("Lightning", log)

log.info("\n"
	"~~~~~~~~~~~~~~~~~~~~~~~~~\n"
	"~ Looking for Lightning ~\n"
	"~~~~~~~~~~~~~~~~~~~~~~~~~"
	"\n"
)

HOST = socket.gethostname()
sublocation = CONF["flux"]["sublocation"]

lightConf = CONF["lightning"]
ADDRESS = lightConf["address"]
PIN = lightConf["pin"]

RpLi = RPi_AS3935.RPi_AS3935

def stormWatch(channel):
	log.debug("Saw something...")
	# Sleep for a bit to warm up the sensor
	sleep(0.003)

	global sensor
	try:
		reason = sensor.get_interrupt()
	except TimeoutError as te:
		log.error("AS3935 Sensor not found, unable to get interrupt, please check connection at {0}".format(ADDRESS))
		log.error(te)
		mqtt.publishError("AS3935 Sensor not found", True)
		return

	try:
		sensor.set_mask_disturber(True)
	except OSError as oe:
		log.error("AS3935 Sensor not found, unable to set mask, please check connection at {0}".format(ADDRESS))
		log.error(oe)
		mqtt.publishError("AS3935 Sensor not found", True)

	if(reason == 0x01):
		log.info("Noise Level too high - adjusting")
		sensor.raise_noise_floor()
	elif(reason == 0x04):
		log.info("Disturber detected - masking")
		sensor.set_mask_disturber(True)
	elif(reason == 0x08):
		distance = sensor.get_distance()
		energy = sensor.get_energy()
		log.info("Lightning found! - {0}km away at {1}".format(distance, energy))
		
		now2 = datetime.now().strftime("%Y-%m-%d__%H-%M-%S")
		jsonInfo = {
			"type":"lightning",
			"reason":reason,
			"energy":energy,
			"distance":distance,
			"when":now2
		}
		
		mqtt.publishMessage("weather", json.dumps(jsonInfo))
		point = "lightning,location=outside,sublocation={0},host={1} distance={2},energy={3}".format(sublocation, HOST, distance, energy)
		mqtt.publishMessage("in/lightning", point)
		status.sendStatus(mqtt, "storm", True)

	
log.debug("Setting Indoor: {0}".format(lightConf["indoors"]))
log.debug("Looking for Lightning on pin: {0} | i2c: {1}".format(PIN, ADDRESS))


sensor = RpLi(address=0x03, bus=1)

try:
	sensor.set_indoors(lightConf["indoors"])
	sensor.set_noise_floor(0)
	log.debug("Setting Noise Floor")
	sensor.calibrate(tun_cap=0x0F)
except(OSError, TimeoutError) as oe1:
		mqtt.publishError("AS3935 Sensor not initalized", True)

GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN, GPIO.IN)
log.debug("Adding Event Detection on pin: {0}".format(PIN))

# lightShow = Button(PIN)
log.debug("Connecting to event")
# lightShow.when_pressed = stormWatch

GPIO.add_event_detect(PIN, GPIO.RISING, callback=stormWatch)

status.sendStatus(mqtt, "storm", True)

# lightning.startSearch(log)
while True:
	sleep(0.1)