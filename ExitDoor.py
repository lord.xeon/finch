# Exit script when services stop
# 
from time import sleep
import json
import socket
import logging
import argparse
import sys

from src import logger, config, status, mqtt

CONF = config.loadConfig("/home/pi/weather")
exitConf = CONF["exit"]
sublocation = CONF["flux"]["sublocation"]
HOST = socket.gethostname()

log = logger.beginLogging(exitConf["log"])

mqtt.startConnection("ExitDoor", log)

log.info(
	"\n"
	"================================\n"
	"|| Starting Exit Door Service ||\n"
	"================================"
	"\n"
)

parser = argparse.ArgumentParser(description='Exit Service for Weather Tracking.')

# The Service name
parser.add_argument("-s", "--service_name", help="Service name to exit.")
# Should we set the status to offline or online - defaults to off
parser.add_argument("-o", "--online_status", help="Is the service online or offline", action="store_true", default=False)

args = parser.parse_args()

# If no service is passed in, return
if(args.service_name is None):
	sys.exit("Error - No service provided")

# Just in case something goes wrong sending the status, 
try:
	status.sendStatus(mqtt, args.service_name, args.online_status)
	log.info("Setting {0} status on {1} service".format(args.online_status, args.service_name))
except (m) as e:
	sys.exit("Error - Problem No service provided")
	raise e

sys.exit()
