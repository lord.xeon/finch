# Snow Total Logging
# 
from time import sleep
import json
import socket
# import logging

from src import snow, mqtt, logger, config

CONF = config.loadConfig("/home/pi/weather")
snowInfo = CONF["snow"]
sublocation = CONF["flux"]["sublocation"]

log = logger.beginLogging(snowInfo["log"])
mqtt.startConnection("Snow", log)
HOST = socket.gethostname()


log.info(
	"\n"
	"===========================\n"
	"|| Starting Snow Service ||\n"
	"==========================="
	"\n"
)

def startSnow(client, userData, message):
	print("data:")
	print(userData)
	print("Got message!")
	print(message)

mqtt.subscribeTo("control/snow", startSnow)

sleepTime = snowInfo["sleep"] * 60
baseHeight = snowInfo["baseHeight"]



while True:
	print(snow.measure(log))
	sleep(1000)
