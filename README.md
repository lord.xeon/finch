# Finch

Self hosted Raspberry Pi weather station.

## Getting started

This project, with the help of some sensors, builds a raspberry pi powered weather station that tracks the following metrics:

- Air Temperature
- Air Humidity Level
- Air Pressure
- UV Index
- Wind Speed
- Rainfall amount
- Lightning Strikes
- Snowfall level

Using the metrics above you can also calculate the Wind Chill in the winter, and Feels Like temperature in the summer, and Dew Point all the time.

## Data Flow
While you can use this project by itself, it's real power is combination with other services and technological stacks.  By default this project sends out 2 (two) MQTT messages on each service:
```
weather

JSON object of {
	type:"string",
	otherMetrics,
}
```

The `weather` message is meant to be consumed by web front ends or anything that can consume a JSON object.  It's perfect for a websocket based widget, or for sending to something like [Home Assistant](https://www.home-assistant.io/).

```
in/SERVICE_NAME

Influx DB 2.x Point protocol
```
This message is intended to be consumed by a [Telegraf](https://www.influxdata.com/time-series-platform/telegraf/) service to send to an Influx DB.  Once the data is in Influx, you can do an infinite number of things with it.

![Data Flow](Finch-Flow.jpg)

### Example Messages
```
weather:
{
	"type": "temperature",
	"temperature": {
		"c": "-4.21",
		"f": "24.42"
	},
	"dewPoint": {
		"c": "10.83",
		"f": "51.50"
	},
	"humidity": "45.83"
}

in/temperature
weather,location={config},sublocation={config},host={hostname} temperature=-4.21,humidity=45.83,dewpoint=10.83
```
The Dew Point is calculated and saved.  The `weather` message includes temperature in both Fahrenheit and Celsius.  The data sent to telegraf is in Celsius.  Rainfall, snowfall, and temperature save data in metric.  Windspeed is calculated in miles per hour and saved that way.

## Raspberry Pi
This project uses a Raspberry Pi v3, though a Pi Zero probably has more then enough compute power to run everything.  You could likely use an ESP32 also if you want an even lower powered design.  This project is hard wired to mains power meaning battery life was not an issue.

## Sensors
This project makes uses of individual sensors to track the above metrics and runs a python script as a service for each metric.  Air Temperature, Air Humidity, and UV Index are run on a basic interval (configurable).  Wind Speed is calculated over a 10 second time frame for 60 seconds total.  If any speed is registered, it will be sent.  Rain fall is event based and will calculate the total number of buckets tripped over an interval (configurable).  Lightning is completely event based, whenever a lightning strike is detected an event is called.  Snowfall is similar to rainfall, however it is run manually (when snow is in the forcast).

Scripts are generally located at `/lib/systemd/system/`


### Temp & Humidity
This project uses an `AHT20` sensor to detect temperature and humidity.  Specifically it's an `AM2315C`, the waterproof `i2c` style.

### Pressure
This project uses an `MPR Micropressure` sensor to detect the air pressure.  Hooked up to a tube, we can keep the sensor in a waterproof box while the tube is exposed to the air for readings.  This should prove more stable than earlier attempts with a `BME280`

### UV
This project uses a `veml6075` sensor to detect the UV value.

### Wind Speed
Any digital anemometer will work, but this project uses a [Davis Instruments Anemometer](https://www.davisinstruments.com/products/anemometer-for-vantage-pro2-vantage-pro).  This aneometer also has a wind direction sensor, but I have now hooked it up.  It requires an ADC which I didn't have handy, but more importantly it requires calibration of the initial direction of the vane.

### Rainfall
Any digital tipping bucket/spoon rainfall sensor will work, but this project uses a [Davis Instuments Tipping Spoon](https://www.davisinstruments.com/products/aerocone-rain-collector-with-vantage-pro2-mounting-base).

### Lightning Sensor
This project uses an `AS3935` sensor to detect lightning strikes.  While it can detect the "energy" burst given, (and we do track that), it's not really useful information because it's estimated.

### Snowfall
This project uses an ultrasonic sensor to determine the distance between the sensor bottom and the ground.

### System Info
As part of the temp & humidity service, we also track and send through the system uptime and system temperature.  They get sent as MQTT messages to `system` as a JSON object, and to `in/system` as an Influx DB point.

## Logging
Each script will log to it's own file at start up.  If `debug` is set for a given script, then as you would expect, more information is given during the script's running.  The default log level is `info`, and it includes some basic helpful info for when the scripts begin to make sure that things are working correctly.

## Status & Control
### Online Status
Each system sends a status message via MQTT to the following topic - `status/{hostname}/{service}` with a string of `online` or `offline`.  This is useful for boolean buttons within HomeAssistant or other services to track and notify you the script stops working.  This syntax fits natively with HomeAssistant.

### Control
The Weather Service and the UV service can be controlled via an MQTT message sent to `control/{hostname}` topic with a JSON object of `{ service:"string" }`.  At the moment, we only support getting the current values of each sensor.

---

## Acknowledgments
This project wouldn't be possible without the open source community, in  particular, reading the following helped me with ideas, code, layout, & setup:

* [TowerUp](https:                 //blog.towerup.com/wind-speed-measurements-with-anemometer-and-a-raspberry-pi/) for calculations on the wind speed
* [Coffee With Robots](https:      //coffeewithrobots.com/detecting-lightning-with-a-raspberry-pi/) for lightning detection
* [Geekstips](https:               //www.geekstips.com/arduino-snow-depth-remote-sensing-with-ultrasonic-sensor/) for the idea on how to calculate snow depth
* [Adafruit Circuit Python](https: //learn.adafruit.com/welcome-to-circuitpython), seriously, they have great tutorials and libraries for so many sensors, including many of the ones used here
* [Sparkfun](https:                //learn.sparkfun.com/) for sensors, tutorials, and ideas
* [Paho MQTT](https:               //pypi.org/project/paho-mqtt/) dead simple to set up
* [Diagrams.net](https:            //app.diagrams.net/) for the ace flowchart
* [Stack Overflow](https:          //stackoverflow.com/a/42471883) a beginner's dream

## Disclaimer
None of this code is full time production ready.  Python is not my language of choice.  That being said, I hope that this code can be used as a starting point for other people's self hosted weather stations.


## Future
This is an ongoing project, as I get more time and more ideas I'll add more integrations into HomeAssistant.  I would like to add more sensors such as:
* Wind Direction sensor
* ~~Barometer~~
* Air Quality sensor
* Soil Temperature sensor
* 433Mhz receiver to integrate into self contained commercial sensors