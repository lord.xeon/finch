# Weather info
# 
from time import sleep
import json
import socket
import logging

from src import aht, cpu, mqtt, logger, config, status, ctrl

CONF = config.loadConfig("/home/pi/weather")
weather = CONF["weather"]
sublocation = CONF["flux"]["sublocation"]

log = logger.beginLogging(weather["log"])

HOST = socket.gethostname()

mqtt.startConnection("Weather", log)

log.info(
	"\n"
	"==============================\n"
	"|| Starting Weather Service ||\n"
	"=============================="
	"\n"
)

while True:
	ctrl.getWeather(mqtt, log)
	sleep(weather["sleep"])