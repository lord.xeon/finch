# Control Service
# 
from time import sleep
import json
import socket
import logging

from queue import Queue

from src import ctrl, mqtt, logger, config

CONF = config.loadConfig("/home/pi/weather")
control = CONF["control"]
sublocation = CONF["flux"]["sublocation"]

log = logger.beginLogging(control["log"])

HOST = socket.gethostname()

mqtt.startConnection("Control", log)

log.info(
	"\n"
	"==============================\n"
	"|| Starting Control Service ||\n"
	"=============================="
	"\n"
)

# Set up a Queue to track messages
q = Queue()

def sendUpdate(client, userData, mess):
	q.put(mess)


mqtt.subscribeTo("control/{0}".format(HOST), sendUpdate)


log.info("Starting Queue...")
while True:
	message = q.get()

	rawMessage = str(message.payload.decode("utf-8"))
	obj = json.loads(rawMessage)

	log.debug("Getting info for: {0} service".format(obj["service"]))

	if(obj["service"] == "weather"):
		ctrl.getWeather(mqtt, log)
	elif(obj["service"] == "uv"):
		ctrl.getUV(mqtt, log)
	else:
		log.info("No compatible service found")
