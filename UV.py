# Weather info
# 
from time import time, sleep
import json
import socket
import logging

from src import veml, mqtt, logger, config, status, adafruit_uv, ctrl

CONF = config.loadConfig("/home/pi/weather")
uvConfig = CONF["uv"]
sublocation = CONF["flux"]["sublocation"]

log = logger.beginLogging(uvConfig["log"])

HOST = socket.gethostname()

mqtt.startConnection("UV", log)

log.info(
	"\n"
	"=========================\n"
	"|| Starting UV Service ||\n"
	"========================="
	"\n"
)

INTERVAL = uvConfig["sleep"]
log.info("Gathering UV information every {0} seconds via VEML6075 Sensor".format(INTERVAL))

while True:
	ctrl.getUV(mqtt, log)
	sleep(INTERVAL)